package com.checkpoints.d1.serieA;

import com.utilities.Logger;

public class Heizung {
    private int temperature;
    private Integer min;
    private Integer max;
    private int increment;

    //task a)
    public Heizung(int temperature, int increment) {
        this.temperature = temperature;
        this.increment = increment;
    }

    //task b) old
    public Heizung(Integer min, Integer max) {
        this.temperature = 15;
        this.min = min;
        this.max = max;
    }
    //task b) new
    public static Heizung getHeizungWithMinAndMax(Integer min, Integer max){
        Heizung heizung = new Heizung();
        heizung.setTemperature(15);
        heizung.setMin(min);
        heizung.setMax(max);

        return heizung;
    }

    //task c)
    public Heizung() {
        //leer
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public int getIncrement() {
        return increment;
    }

    public void setIncrement(int increment) {
        this.increment = increment;
    }

    public void increaseTemperature(){
        if(!isUnderMaximum(temperature+increment)) {
            Logger.log("Max Check failed!");
            return;
        }
        temperature += increment;
    }

    public void decreaseTemperature(){
        if(!isAboveMinimum(temperature-increment)) {
            Logger.log("Min Check failed!");
            return;
        }
        temperature -= increment;
    }

    private boolean isAboveMinimum(int temp){
        return min == null || temp >= min;
    }

    private boolean isUnderMaximum(int temp){
        return max == null || temp <= max;
    }

    @Override
    public String toString() {
        return "Heizung{" +
                "temperature=" + temperature +
                ", min=" + min +
                ", max=" + max +
                ", increment=" + increment +
                '}';
    }
}
