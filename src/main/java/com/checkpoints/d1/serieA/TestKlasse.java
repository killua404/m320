package com.checkpoints.d1.serieA;

import com.utilities.Logger;

public class TestKlasse {
    public static void main(String[] args) {

        //task a)
        Logger.log("Task a)\n");

        Heizung heizung1 = new Heizung(20,5);

        Logger.log(heizung1.toString());
        heizung1.increaseTemperature();
        Logger.log(heizung1.toString());

        heizung1.decreaseTemperature();
        Logger.log(heizung1.toString());

        //task b)
        Logger.log("\nTask b)\n");

        Heizung heizung2 = Heizung.getHeizungWithMinAndMax(10,30);
        heizung2.setIncrement(500);
        Logger.log(heizung2.toString());
        heizung2.increaseTemperature();


        //task c)
        Logger.log("\nTask c)\n");

        Heizung heizung3 = new Heizung();
        heizung3.setTemperature(300);
        heizung3.setIncrement(30);
        heizung3.setMax(5000);
        heizung3.setMin(-300);

        Logger.log(heizung3.toString());

    }
}
