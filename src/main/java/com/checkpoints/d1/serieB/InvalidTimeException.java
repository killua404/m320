package com.checkpoints.d1.serieB;

public class InvalidTimeException extends Exception{

    public InvalidTimeException() {
        super("Invalid values for time");
    }
}
