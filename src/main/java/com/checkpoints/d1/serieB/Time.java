package com.checkpoints.d1.serieB;

public class Time {
    private int second;
    private int minute;
    private  int hour;

    public Time(int second, int minute, int hour){

        try{
            isTimeValid(second,minute,hour);
        } catch (InvalidTimeException e) {
            e.printStackTrace();
            return;
        }
        this.second = second;
        this.minute = minute;
        this.hour = hour;
    }

    public Time() {
    }

    public void setTime(int second, int minute, int hour) throws InvalidTimeException{
        try{
            isTimeValid(second,minute,hour);
        } catch (InvalidTimeException e) {
            e.printStackTrace();
            return;
        }
        this.second = second;
        this.minute = minute;
        this.hour = hour;
    }

    public Time nextSecond(){
        if(this.second == 59){
            this.minute+=1;
        }

        if(this.minute == 59){
            this.hour +=1;
        }

        if(this.hour==24){
            this.minute = 0;
            this.second = 0;
            this.hour = 0;
        }

        return this;
    }

    private boolean isTimeValid(int second, int minute, int hour) throws InvalidTimeException{
        if(second>59 || second<0){
            throw new InvalidTimeException();
        }
        if(minute>59 || minute<0){
            throw new InvalidTimeException();
        }
        if(hour>23 || hour<0){
            throw new InvalidTimeException();
        }
        return true;
    }
    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    @Override
    public String toString() {
        String timeString = "";

        if(hour<10){
            timeString+="0";
        }
        timeString+=this.hour+":";



        if(minute<10){
            timeString+="0";
        }
        timeString+=this.minute+":";

        if(second<10){
            timeString+="0";
        }
        timeString+=this.second;

        return timeString;
    }
}
