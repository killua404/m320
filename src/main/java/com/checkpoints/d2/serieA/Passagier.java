package com.checkpoints.d2.serieA;

import com.utilities.Logger;

public class Passagier {
    private String name;

    public Passagier(String name) {
        this.name = name;
    }
    /**
     * Prints the name of the passenger
     */
    public void nameAusgeben() {
        Logger.log(name);
    }
}
