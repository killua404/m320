package com.checkpoints.d2.serieA;

import com.utilities.Logger;

import java.util.ArrayList;
import java.util.List;

public class Flug {
    private List<Passagier> passagierList = new ArrayList<Passagier>();

    /**
     * Adds the passenger to the flight
     * @param p a Passenger
     */
    public void addPassagierToFlug(Passagier p){
        passagierList.add(p);
    }
    /**
     * Removes the passenger from the flight
     * @param p a Passenger
     */
    public void removePassagier(Passagier p){
        passagierList.remove(p);
    }
    /**
     * Prints the passengerList
     */
    public void passagierListAusgeben(){
        Logger.log("Passagierliste:");
        for (Passagier p: passagierList){
            p.nameAusgeben();
        }
    }
}
