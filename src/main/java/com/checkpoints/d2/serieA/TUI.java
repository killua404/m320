package com.checkpoints.d2.serieA;

import com.utilities.Logger;

import java.util.Scanner;

public class TUI {
    static Scanner scanner = new Scanner(System.in);
    static private Passagier p;
    static private Flug f = new Flug();

    /**
     * Starts the TUI
     */
    public static void start() {

        String name = getPassagierName();
        p = new Passagier(name);
        int choice=0;
        while (choice!=4) {
            Logger.lognl("[1] Zum Flug hinzufügen\n[2] Vom Flug entfernen\n[3] Liste ausgeben\n[4] Abbrechen\n\nInput: ");
            choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    f.addPassagierToFlug(p);
                    break;
                case 2:
                    f.removePassagier(p);
                    break;
                case 3:
                    f.passagierListAusgeben();
                    break;
                case 4:
                    Logger.log("Bye Bye");
                    break;
                default:
                    Logger.log("Invalid input");
                    break;
            }
        }
    }
    /**
     * Asks for the name of the passenger
     * @return Name of the passenger
     */
    private static String getPassagierName() {
        Logger.lognl("Bitte geben Sie Ihren Namen ein: ");
        return scanner.nextLine();
    }
}
