package com.checkpoints.v1.serieA;

import com.utilities.Logger;

public class TestKlasse {


    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<Integer>();

        stack.push(45);
        stack.push(234342);
        stack.show();
        Logger.log(stack.pop().toString());
        Logger.log(stack.pop().toString());
        stack.show();

        stack.push(45);
        stack.push(234342);
        stack.push(324324);
        stack.push(1);

        while (stack.hasNext()){
            Logger.log(stack.next().toString());
            stack.show();
        }
        stack.show();

        while (stack.hasNext()){
            Integer temp = stack.next();
            Logger.log(temp.toString());
            if(temp == 234342){
                stack.remove();
            }
            stack.show();
        }



    }
}
