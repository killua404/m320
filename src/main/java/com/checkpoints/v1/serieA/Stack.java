package com.checkpoints.v1.serieA;

import com.utilities.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Stack<Type> implements Iterator {
    List<Type> items;
    
    private int current;
    
    public Stack() {
        this.items = new ArrayList<Type>();
        this.current = -1;
    }

    public void push(Type obj){
        items.add(obj);
        current++;
    }

    public Type pop(){
        Type temp = this.items.get(items.size()-1);
        this.items.remove(temp);
        current--;
        return temp;
    }

    public void show(){
        Logger.log(this.toString());
    }

    @Override
    public boolean hasNext() {
        if(current<0){
            //reset
            current = this.items.size()-1;
            return false;
        }
        return true;
    }

    @Override
    public Type next() {
        current--;
        return items.get(current+1);
    }

    @Override
    public void remove() {
        this.items.remove(current+1);
    }

    @Override
    public String toString() {
        return "Stack{" +
                "items=" + items +
                ", current=" + current +
                '}';
    }

}
