package com.checkpoints.d3;

import com.utilities.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FileSearcher {

    public static List<File> searchFile(File dir,String searchParameter) throws NoFilesFoundException {
        List<File> result = searchFiles(dir,searchParameter);

        if(result.isEmpty()){
            throw new NoFilesFoundException("There are no files matching the input");
        }

        return result;
    }

    public static List<File> searchFiles(File dir, String searchParameter){
        List<File> resultList = new ArrayList<>();

        resultList.addAll(getFilesOfFolder(dir,searchParameter));


        getSubFolders(dir,searchParameter).forEach((subdir) -> {
            try {
                resultList.addAll(searchFile(subdir,searchParameter));
            } catch (NoFilesFoundException e) {
                Logger.log("Subfolder [" + subdir + "] has no matching files");
            }
        });

        return resultList;
    }

    public static List<File> getFilesOfFolder(File dir, String searchParameter){
        List<File> files = new ArrayList<>(List.of(Objects.requireNonNull(dir.listFiles())));

        /*
        files.forEach((file) -> {
            if(!file.getName().contains(searchParameter)){
                files.remove(file);
            }
        });*/
        files.removeIf(file -> !file.getName().contains(searchParameter));

        return files;
    }
    public static List<File> getSubFolders(File dir, String searchParameter){
        List<File> files = new ArrayList<>(List.of(Objects.requireNonNull(dir.listFiles())));

        /*
        files.forEach((file) -> {
            if(!file.isDirectory()){
                files.remove(file);
            }
        });*/
        files.removeIf(file -> !file.isDirectory());

        return files;
    }
}
