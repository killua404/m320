package com.checkpoints.d3;

import com.utilities.Logger;

import java.io.File;
import java.util.List;

public class Testklasse {
    public static void main(String[] args) {
        File dir = new File("C:\\Users\\School\\Documents\\Module");

        List<File> resultList = null;
        try {
            resultList = FileSearcher.searchFile(dir,"M320");

            resultList.forEach((file) -> {
                Logger.log(file.toString());
            });
        } catch (NoFilesFoundException e) {
            Logger.log("No Files found matching the searchparameter");
        }


    }
}
