package com.checkpoints.d3;

public class NoFilesFoundException extends Exception{
    NoFilesFoundException(String errorMessage){
        super(errorMessage);
    }
}
