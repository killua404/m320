package com.checkpoints.aufgabe.exceptions;

public class DirNotDefined extends Exception{

    public DirNotDefined(String errormsg){
        super(errormsg);
    }
}
