package com.checkpoints.aufgabe.exceptions;

public class DirDoesNotExist extends Exception{
    public DirDoesNotExist(String errorMessage){
        super(errorMessage);
    }
}
