package com.checkpoints.aufgabe.exceptions;

public class NoFilesFoundException extends Exception{
    public NoFilesFoundException(String errorMessage){
        super(errorMessage);
    }
}
