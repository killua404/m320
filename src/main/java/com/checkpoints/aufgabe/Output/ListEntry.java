package com.checkpoints.aufgabe.Output;

import com.utilities.Logger;

import java.io.File;

public class ListEntry {

    File file;

    public ListEntry(File file) {
        this.file = file;
    }

    public String toRowString(int maxFileLength, int maxPathLength) {
        String row = "";
        int diffFile = maxFileLength - file.getName().length();
        int diffPath = maxPathLength - file.getPath().length();
        //Logger.lognl(file.getPath() + file.getName());
        if(diffFile>0) {
            row += "| " + String.format("%-" + maxFileLength + "s", file.getName()) + " | ";
        }
        else {
            row += "| " + file.getName() + " | ";
        }

        if(diffPath>0) {
            row += String.format("%-" + maxPathLength + "s", file.getPath()) + " |";
        }
        else {
            row += file.getPath() + " |";
        }
        return row + "\n";
    }
}
