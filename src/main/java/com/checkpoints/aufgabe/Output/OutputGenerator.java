package com.checkpoints.aufgabe.Output;

import com.utilities.Logger;

import java.io.File;
import java.util.List;

public class OutputGenerator {

    public static void printOutput(List<File> fileList) {
        Logger.log(generateOutput(fileList));
    }

    public static String generateOutput(List<File> fileList) {
        int maxFileLength = getMaxFileStringLength(fileList);
        int maxPathLength = getMaxPathStringLength(fileList);

        StringBuilder list = new StringBuilder();
        list.append(generateHeader(maxFileLength,maxPathLength));
        for (File file : fileList) {
            list.append(new ListEntry(file).toRowString(maxFileLength, maxPathLength));
        }

        return list.toString();
    }

    public static String generateHeader(int maxFileLength, int maxPathLength) {

        StringBuilder header = new StringBuilder();

        header.append("| " + String.format("%-" + maxFileLength + "s", "Name") + " | ");
        header.append(String.format("%-" + maxPathLength + "s", "Path") + " | ");

        header.append("\n");

        header.append("| " + String.format("%-" + maxFileLength + "s", "").replace(" ", "-") + " | ");
        header.append(String.format("%-" + maxPathLength + "s", "").replace(" ", "-") + " | ");

        header.append("\n");

        return header.toString();
    }

    public static int getMaxFileStringLength(List<File> fileList) {
        int maxFileLength = 4;
        int tempFileLength;

        for (File file : fileList) {
            tempFileLength = file.getName().length();
            if (maxFileLength < tempFileLength) {
                maxFileLength = tempFileLength;
            }

        }
        return maxFileLength;
    }



    public static int getMaxPathStringLength(List<File> fileList) {
        int maxPathLength = 4;
        int tempPathLength;

        for (File file : fileList) {
            tempPathLength = file.getPath().length();
            if (maxPathLength < tempPathLength) {
                maxPathLength = tempPathLength;
            }

        }
        return maxPathLength;
    }


}
