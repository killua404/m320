package com.checkpoints.aufgabe;

import com.checkpoints.aufgabe.Output.OutputGenerator;
import com.checkpoints.aufgabe.exceptions.DirDoesNotExist;
import com.checkpoints.aufgabe.exceptions.NoFilesFoundException;
import com.checkpoints.aufgabe.searcher.Directory;
import com.checkpoints.aufgabe.searcher.FileSearcher;
import com.utilities.Logger;

import java.io.File;
import java.util.List;

public class Tests {
    public static void main(String[] args) {
        FileSearcher fs = new FileSearcher();
        List<File> resultList = null;
        try {
            //resultList = fs.searchFile( new File("C:\\Users\\School\\Documents\\Module"),"be",false);
            resultList = fs.searchFile( new File("C:\\Users\\Beni\\Documents\\Git\\m320"),"be",false);
            /*resultList.forEach((file) -> {
                Logger.log(file.toString());
            });*/
            OutputGenerator.printOutput(resultList);
        } catch (NoFilesFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            Directory.setDirectory("C:\\Users\\Beni\\Documents\\Git\\m320");
        } catch (DirDoesNotExist e) {
            throw new RuntimeException(e);
        }
    }
}
