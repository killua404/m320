package com.checkpoints.aufgabe.searcher;

import com.checkpoints.aufgabe.exceptions.DirDoesNotExist;
import com.checkpoints.aufgabe.exceptions.DirNotDefined;
import com.utilities.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Directory {
    private static File instance;

    public static File getInstance() throws DirNotDefined {
        if(instance == null){
            throw new DirNotDefined("Define the directory first");
        }else {
            return instance;
        }
    }

    public static void setDirectory(String dirPath) throws DirDoesNotExist{
        /*try {
            Path p = Files.createTempDirectory(dirPath);
            if (!Files.exists(p)){
                throw new DirDoesNotExist("Dir does not exist");
            }
        } catch (IOException e) {
            throw new DirDoesNotExist("Dir does not exist");
        }*/
        File temp = new File(dirPath);

        if(temp.exists()) {
            instance = new File(dirPath);
        } else {
            Logger.lognl("This directory does not exist");
        }
    }
}
