package com.checkpoints.aufgabe.searcher;

import com.checkpoints.aufgabe.exceptions.NoFilesFoundException;
import com.checkpoints.aufgabe.interfaces.ISearchable;
import com.utilities.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FileSearcher implements ISearchable {

    @Override
    public List<File> searchFile(File dir, String searchParameter, boolean logging) throws NoFilesFoundException {
        List<File> result = searchFiles(dir,searchParameter, logging);

        if(result.isEmpty()){
            throw new NoFilesFoundException("There are no files matching the input");
        }

        return result;
    }

    @Override
    public List<File> searchFile(File dir, String searchParameter, String fileExtension, boolean logging) throws NoFilesFoundException {
        Logger.lognl("file extension");
        return List.of();
    }

    private List<File> searchFiles(File dir, String searchParameter, boolean logging){
        List<File> resultList = new ArrayList<>();

        resultList.addAll(getFilesOfFolder(dir,searchParameter));

        getSubFolders(dir,searchParameter).forEach((subdir) -> {
            try {
                resultList.addAll(searchFile(subdir,searchParameter, logging));
            } catch (NoFilesFoundException e) {
                if(logging) {
                    Logger.log("Subfolder [" + subdir + "] has no matching files");
                }
            }
        });

        return resultList;
    }

    private List<File> getFilesOfFolder(File dir, String searchParameter){
        List<File> files = new ArrayList<>(List.of(Objects.requireNonNull(dir.listFiles())));


        files.removeIf(file -> !file.getName().contains(searchParameter));

        return files;
    }
    private List<File> getSubFolders(File dir, String searchParameter){
        List<File> files = new ArrayList<>(List.of(Objects.requireNonNull(dir.listFiles())));

        files.removeIf(file -> !file.isDirectory());
        files.removeIf(file -> file.getName().startsWith("."));

        return files;
    }
}
