package com.checkpoints.aufgabe;

import com.checkpoints.aufgabe.Output.OutputGenerator;
import com.checkpoints.aufgabe.exceptions.DirDoesNotExist;
import com.checkpoints.aufgabe.exceptions.DirNotDefined;
import com.checkpoints.aufgabe.exceptions.NoFilesFoundException;
import com.checkpoints.aufgabe.searcher.Directory;
import com.checkpoints.aufgabe.searcher.FileSearcher;
import com.utilities.Logger;

import java.io.File;
import java.util.List;
import java.util.Scanner;

public class TUI {
    private Scanner scanner = new Scanner(System.in);
    private FileSearcher fileSearcher = new FileSearcher();
    public void start() {

        String command = "";
        while (!command.equals("exit")) {
            Logger.lognl("\nInput: ");
            command = scanner.nextLine();
            switch (command.split(" ")[0]) {
                case "cd":
                    String path = command.split(" ")[1];
                    Logger.lognl(path);
                    try {
                        Directory.setDirectory(path);
                    } catch (DirDoesNotExist e) {
                        Logger.lognl("Directory does not exist");
                    }
                    break;
                case "search":
                    List<File> resultList = null;
                    try {
                        String searchparam = command.split(" ")[1];

                        resultList = fileSearcher.searchFile(Directory.getInstance(),searchparam, false);

                        /*resultList.forEach((file) -> {
                            Logger.log(file.toString());
                        });*/
                        OutputGenerator.printOutput(resultList);

                    } catch (DirNotDefined e) {
                        Logger.lognl("Define the directory first with: 'cd [dir]'");
                    } catch (NoFilesFoundException e) {
                        Logger.lognl("No Files found matching the search param");
                    }

                    break;
                case "help":
                    Logger.lognl("Options:\nSet Directory: cd [Path]\nSearch: search [search param]\nShow this list: help\nExit: exit\n");
                    break;
                case "placeholder2":
                    Logger.log("Placeholder2");
                    break;
                case "exit":
                    Logger.log("Bye Bye");
                    break;
                default:
                    Logger.log(command + " is not recognized as a valid command");
                    break;
            }
        }

    }
}
