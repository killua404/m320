package com.checkpoints.aufgabe.interfaces;

import com.checkpoints.aufgabe.exceptions.NoFilesFoundException;

import java.io.File;
import java.lang.reflect.Field;
import java.util.List;

public interface ISearchable {
    public List<File> searchFile(File dir, String searchParameter, boolean logging) throws NoFilesFoundException;
    public List<File> searchFile(File dir, String searchParameter,String fileExtension, boolean logging) throws NoFilesFoundException;
}
