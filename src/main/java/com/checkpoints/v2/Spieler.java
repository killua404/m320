package com.checkpoints.v2;

import com.utilities.Logger;

abstract public class Spieler {
    protected String name;

    public Spieler(String name) {
        this.name = name;
    }

    public void zeigeName(){
        Logger.log(this.name);
    }

    abstract void spielen();
}
