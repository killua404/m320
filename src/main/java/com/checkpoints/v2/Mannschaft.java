package com.checkpoints.v2;

import java.util.ArrayList;
import java.util.List;

public class Mannschaft {
    private List<Spieler> spielerList = new ArrayList<Spieler>();

    public void add(Spieler p){
        spielerList.add(p);
    }

    public List<Spieler> getSpielerList() {
        return spielerList;
    }
}
