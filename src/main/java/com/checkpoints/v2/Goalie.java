package com.checkpoints.v2;

import com.utilities.Logger;

public class Goalie extends Spieler{
    private int koerperGroesse;

    public Goalie(String name, int koerperGroesse) {
        super(name);
        this.koerperGroesse = koerperGroesse;
    }

    @Override
    void spielen() {
        Logger.log("Ball fangen");
    }
}
