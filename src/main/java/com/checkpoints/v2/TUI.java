package com.checkpoints.v2;

import com.utilities.Logger;

import java.util.Scanner;

public class TUI {
    static private Mannschaft mannschaft = new Mannschaft();
    static Scanner scanner = new Scanner(System.in);
    public static void start() {

        int choice = 0;
        while (choice != 5) {
            Logger.lognl("Optionen:\n[1] Neuen Goalie\n[2] neuen Angreifer\n[3] neuen Verteidiger\n[4] Spielen\n[5] Abbruch\n\nInput: ");
            choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    createAndAddGoalie();
                    break;
                case 2:
                    createAndAddAngreifer();
                    break;
                case 3:
                    createAndAddVerteidiger();
                    break;
                case 4:
                    Logger.log("Spielen:");
                    mannschaftSpielenLassen();
                    break;
                case 5:
                    Logger.log("Bye Bye");
                    break;
                default:
                    Logger.log("Invalid input");
                    break;
            }
        }
    }

    private static void createAndAddVerteidiger() {
        String name = getName();
        Verteidiger verteidiger = new Verteidiger(name);
        mannschaft.add(verteidiger);
    }

    private static void createAndAddAngreifer() {
        String name = getName();

        Angreifer angreifer = new Angreifer(name);
        mannschaft.add(angreifer);
    }

    private static void createAndAddGoalie(){

        String name = getName();
        int koerpergroesse = getGroesse();
        Goalie goalie = new Goalie(name,koerpergroesse);
        mannschaft.add(goalie);
    }

    private static void mannschaftSpielenLassen(){
        for(Spieler spieler : mannschaft.getSpielerList()){
            spieler.spielen();
        }
    }

    private static String getName(){
        Logger.lognl("Name: ");
        return scanner.next();
    }
    private static int getGroesse(){
        Logger.lognl("Groesse: ");
        return scanner.nextInt();
    }
}
