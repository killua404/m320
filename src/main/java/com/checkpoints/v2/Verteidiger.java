package com.checkpoints.v2;

import com.utilities.Logger;

public class Verteidiger extends Spieler{
    public Verteidiger(String name) {
        super(name);
    }

    @Override
    void spielen() {
        Logger.log("Goal verteidigen");
    }
}
