package com.checkpoints.v2;

import com.utilities.Logger;

public class Angreifer extends Spieler{
    public Angreifer(String name) {
        super(name);
    }

    @Override
    void spielen() {
        Logger.log("Auf Goal schiessen");
    }

    public void jogTraining(){
        Logger.log("Jogging");
    }
}
