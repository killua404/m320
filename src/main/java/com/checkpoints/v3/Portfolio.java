package com.checkpoints.v3;

import com.checkpoints.v3.StockExchanges.NewYork;
import com.checkpoints.v3.StockExchanges.Zurich;
import com.utilities.Logger;

import java.util.ArrayList;
import java.util.List;

public class Portfolio {
    private StockExchange stockExchange = new Zurich();
    private List<Share> shares = new ArrayList<Share>();

    public Portfolio(){
        Share s1 = new Share("Microsoft",1.0);
        Share s2 = new Share("Tesla",1.0);

        shares.add(s1);
        shares.add(s2);
    }

    public void printPortfolio(){
        for (Share s: shares){
            Logger.log(s.toString());
            Logger.log("Price:"+ stockExchange.getPriceOfShare(s.getName()));
        }
    }

}
