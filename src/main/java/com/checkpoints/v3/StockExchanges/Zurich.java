package com.checkpoints.v3.StockExchanges;

import com.checkpoints.v3.Searcher;
import com.checkpoints.v3.Share;
import com.checkpoints.v3.ShareInformation;
import com.checkpoints.v3.StockExchange;

import java.util.ArrayList;
import java.util.List;

public class Zurich implements StockExchange {
    private List<ShareInformation> shareInformationList = new ArrayList<ShareInformation>();
    public Zurich(){
        ShareInformation Microsoft = new ShareInformation("Microsoft",120.00,"CHF");
        ShareInformation Tesla = new ShareInformation("Tesla",200.00,"CHF");

        shareInformationList.add(Microsoft);
        shareInformationList.add(Tesla);
    }
    @Override
    public double getPriceOfShare(String name) {
        return Searcher.searchInShareList(shareInformationList,name).getPrice();
    }
}
