package com.checkpoints.v3.StockExchanges;

import com.checkpoints.v3.Searcher;
import com.checkpoints.v3.ShareInformation;
import com.checkpoints.v3.StockExchange;

import java.util.ArrayList;
import java.util.List;

public class NewYork implements StockExchange {
    private List<ShareInformation> shareInformationList = new ArrayList<ShareInformation>();
    public NewYork(){
        ShareInformation Microsoft = new ShareInformation("Microsoft",100.00,"USD");
        ShareInformation Tesla = new ShareInformation("Tesla",170.00,"USD");

        shareInformationList.add(Microsoft);
        shareInformationList.add(Tesla);
    }

    @Override
    public double getPriceOfShare(String name) {
        return Searcher.searchInShareList(shareInformationList,name).getPrice();
    }
}
