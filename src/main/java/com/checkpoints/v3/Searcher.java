package com.checkpoints.v3;

import java.util.List;

public class Searcher {
    public static ShareInformation searchInShareList(List<ShareInformation> shareInformationList,String name){
        for (ShareInformation s : shareInformationList){
            if(s.getName().equals(name)){
                return s;
            }
        }
        return null;
    }
}
