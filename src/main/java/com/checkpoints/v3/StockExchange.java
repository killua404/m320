package com.checkpoints.v3;

public interface StockExchange {
    public double getPriceOfShare(String name);
}
