package com.checkpoints.q3;

import com.utilities.Logger;

import java.util.HashMap;
import java.util.Map;

public class Airports {
    private HashMap<String,String> airportList;

    public Airports(){
        airportList = DataMapper.retrievePairs();
    }

    public void add(String AirportCode, String AirportName){
        airportList.put(AirportCode,AirportName);
    }

    public void remove(String airportCode){
        String airportName = airportList.remove(airportCode);
        Logger.log("Removed: " + airportName);
    }

    public String searchByCode(String AirportCode){
        return airportList.get(AirportCode);
    }

    public void show(){
        /*for (Map.Entry<String,String> entry:airportList.entrySet()){
            Logger.log(entry.getKey()+" --> " + entry.getValue());
        }*/
        airportList.forEach((airportCode, airportName) -> {
            Logger.log(airportCode + " --> " + airportName);
        });
    }
}
