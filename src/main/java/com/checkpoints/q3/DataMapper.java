package com.checkpoints.q3;

import com.utilities.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class DataMapper {
    public static String readFileToString() {
        File dataFile = new File("C:\\Users\\School\\Documents\\Module\\M320\\m320\\src\\main\\java\\com\\checkpoints\\q3\\data\\input.json");
        Scanner scanner = null;
        try {
            scanner = new Scanner(dataFile);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        scanner.useDelimiter("\\Z");
        String json = scanner.next();

        return json;
    }

    public static HashMap<String,String> retrievePairs(){
        String json = readFileToString();
        List<String> pairs = Arrays.stream(json.split("},")).toList();
        HashMap<String,String> map = new HashMap<String,String>();

        for (String s:pairs) {
            String airportCode = s.split("\"AIRPORT CODE\": \"")[1].split("\",")[0];
            String airportName = s.split("\"AIRPORT\": \"")[1].split("\"")[0];

            map.put(airportCode,airportName);
        }
        return map;
    }
}
