package com.checkpoints.q3;

import com.utilities.Logger;

public class TestKlasse {
    public static void main(String[] args) {
        Airports a = new Airports();

        a.add("ABC","ABCTEST");
        a.show();

        a.remove("ABC");
        String result = a.searchByCode("ABQ");
        Logger.log(result);
    }
}
