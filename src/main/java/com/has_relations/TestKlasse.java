package com.has_relations;

import com.utilities.Logger;

public class TestKlasse {
    public static void main(String[] args) {
        Person p1 = new Person("Gregory","Frey");
        Person p2 = new Person("Mike", "Graf");
        Person p3 = new Person("Benjamin","Bosshard");

        Contact c1 = new Contact("Hans","hans@gmail.com",765437867);
        Contact c2 = new Contact("Peter","peter@gmail.com",765437367);
        Contact c3 = new Contact("Ursula","ursula@gmail.com",795437867);

        p1.addContact(c1);
        p1.addContact(c2);

        p2.addContact(c2);
        p2.addContact(c3);

        p3.addContact(c1);
        p3.addContact(c3);

        Logger.log(p1.toString());
        Logger.log(p2.toString());
        Logger.log(p3.toString());
    }
}
