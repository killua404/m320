package com.overriding;

import com.utilities.Logger;

public class Angestellter extends Person{
    private int monatsLohnEuroCent;
    private double stellenProzente;

    public Angestellter(String name, String address, int monatsLohnEuroCent, double stellenProzente) {
        super(name, address);
        this.monatsLohnEuroCent = monatsLohnEuroCent;
        this.stellenProzente = stellenProzente;
    }

    @Override
    public void ausgeben() {
        Logger.log(this.toString());
    }

    public int getMonatsLohnEuroCent() {
        return monatsLohnEuroCent;
    }

    public void setMonatsLohnEuroCent(int monatsLohnEuroCent) {
        this.monatsLohnEuroCent = monatsLohnEuroCent;
    }

    public double getStellenProzente() {
        return stellenProzente;
    }

    public void setStellenProzente(double stellenProzente) {
        this.stellenProzente = stellenProzente;
    }

    @Override
    public String toString() {
        return "Angestellter{" +
                "monatsLohnEuroCent=" + monatsLohnEuroCent +
                ", stellenProzente=" + stellenProzente +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
