package com.overriding;

import com.utilities.Logger;

public class Kunde extends Person{
    private int kundenNummer;
    public Kunde(String name, String address, int kundenNummer) {
        super(name, address);
        this.kundenNummer = kundenNummer;
    }

    @Override
    public void ausgeben() {
        Logger.log(this.toString());
    }

    public int getKundenNummer() {
        return kundenNummer;
    }

    public void setKundenNummer(int kundenNummer) {
        this.kundenNummer = kundenNummer;
    }

    @Override
    public String toString() {
        return "Kunde{" +
                "kundenNummer=" + kundenNummer +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
