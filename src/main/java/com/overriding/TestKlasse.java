package com.overriding;

import java.util.ArrayList;
import java.util.List;

public class TestKlasse {
    public static void main(String[] args) {
        List<Person> people = new ArrayList<Person>();

        Person p1 = new Person("Hans", "Basel");
        Person p2 = new Person("Max","Luzern");
        Kunde k1 = new Kunde("Peter","Zürich",87323874);
        Kunde k2 = new Kunde("Susi","Winterthur",348332342);
        Angestellter a1 = new Angestellter("Luisa","Bern",7878,100.00);
        Angestellter a2 = new Angestellter("Lea","Genf",3000,80.00);
        people.add(p1);
        people.add(p2);
        people.add(k1);
        people.add(k2);
        people.add(a1);
        people.add(a2);

        for(Person p: people){
            p.ausgeben();
        }
    }
}
