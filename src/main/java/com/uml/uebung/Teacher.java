package com.uml.uebung;

public class Teacher extends Person{
    private Assistant assistant;
    public Teacher(Long id, String name, Assistant assistant) {
        super(id, name);
        this.assistant = assistant;
    }
}
