package com.uml.uebung;

public class Person {
    private Long id;
    private String name;

    public Person(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public void showName(){
        System.out.println(this.name);
    }
    public String returnName(){
        return this.name;
    }
}
