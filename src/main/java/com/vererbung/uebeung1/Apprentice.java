package com.vererbung.uebeung1;

import com.utilities.Logger;

public class Apprentice extends Human{
    private String topic;

    public Apprentice(String topic, String name){
        super(name);
        this.topic = topic;

    }

    public void sayHello(){
        Logger.log(this.getName() +" " + this.topic);
    }

}
